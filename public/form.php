<!DOCTYPE html>
<html>
<head>
<title>2 kurs</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <style>

    </style>
</head>

<div class="modal" data-modal="1">
    <?php
    if (!empty($messages)) {
        print('<div id="messages">');
        foreach ($messages as $message) {
            print($message);
        }
        print('</div>');
    }
    ?>
    <body>
	<div class="div">
    <button onclick="document.location='login.php'">Autorisation</button>
    </body>
    <form action="index.php" accept-charset="UTF-8" class="main" method="POST">
        <input style="margin-bottom : 1em" id="formname" type="text" name="fio" placeholder="Name"
            <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>">
        <input style="margin-bottom : 1em;margin-top : 1em" id="formmail" type="email" name="email"
               placeholder="Email"
            <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>">
        <label><br/>
            <input <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year_value']; ?>" id="dr" name="birthyear" value="" type="number" placeholder="Date of Birth:"/>
        </label><br/><br/>


       Sex:<br/>
        <label <?php if($errors['sex']){print 'class="error-radio"';}?>>
            <input <?php if($values['sex_value']=="man"){print 'checked';}?> type="radio"
                                                                             name="radio2" value="man"/>
           Male</label>
        <label <?php if($errors['sex']){print 'class="error-radio"';}?>> <input <?php if($values['sex_value']=="woman"){print 'checked';}?> type="radio"
                                                                                                                                            name="radio2" value="woman"/>
           Femail</label>
        <br/>
        <div <?php if($errors['limb']){print 'class="error-radio-limb"';}?>>
            Number of limbs:<br/>
            <label><input
                    <?php if($values['limb_value']=="1"){print 'checked';}?>
                        type="radio"
                        name="radio1" value="1"/>
                1</label>
            <label><input
                    <?php if($values['limb_value']=="2"){print 'checked';}?>
                        type="radio"
                        name="radio1" value="2"/>
                2</label>
				<label><input
                    <?php if($values['limb_value']=="3"){print 'checked';}?>
                        type="radio"
                        name="radio1" value="3"/>
                3</label>
				<label><input
                    <?php if($values['limb_value']=="10"){print 'checked';}?>
                        type="radio"
                        name="radio1" value="10"/>
                10</label>
				<label><input
                    <?php if($values['limb_value']=="100"){print 'checked';}?>
                        type="radio"
                        name="radio1" value="100"/>
                100!</label>
            <br>
        </div>
        <label>
           Super abilities:
            <br/>
            <div <?php if ($errors['abil']) {print 'class="error-abil"';} ?>> <select id="sp" name="superpower[]"
                   <option value ="0" <?php if(in_array("0", $values['abil_value'])) print "selected";?>>Levitation</option>
                    <option value="1" <?php if(in_array("1", $values['abil_value'])) print "selected";?>>Undead</option>
                    <option value="2" <?php if(in_array("2", $values['abil_value'])) print "selected";?>>Mind reading</option>
                    <option value="3" <?php if(in_array("3", $values['abil_value'])) print "selected";?>>Telekinesis/Paracinesis</option>
                    <option value="4" <?php if(in_array("4", $values['abil_value'])) print "selected";?>>Teleportation</option><br/>
                </select> </div>
        </label><br/>

        <label <?php if ($errors['bio']) {print 'class="error-bio"';} ?>>
            What's up guys?: <br/>
            <textarea id="biog" name="textarea1" placeholder="Пиши тут"><?php print $values['bio_value'];?></textarea>
        </label><br/>

        <div <?php if ($errors['check']) {print 'class="error-check"';} ?>><label><input <?php if($values['check_value']=="1"){print 'checked';}?> style="margin-bottom : 1em;margin-top : 1em;" id="formcheck" type="checkbox" name="checkbox"
                                                                                                                                                   value="1">Согласие на обработку персональных данных</label></div>

        <input type="submit" style="margin-bottom : -1em" id="formsend" class="buttonform" value="Well i read:">
		</div>
    </form>
</div>
<br>
<br>
*Защита от SQL-инъекций — один из распространённых способов взлома сайтов и программ, работающих с базами данных, 
основанный на внедрении в запрос произвольного SQL-кода:<br>
	Использование PDO - расширение для PHP, предоставляющее разработчику универсальный интерфейс для доступа к различным базам данных:<br><br>
	а)<br>	
		$user = 'u35650';<br>
		$pass = '9782638';<br>
		$db = new PDO('mysql:host=localhost;dbname=u35650', $user, $pass, array(PDO::ATTR_PERSISTENT => true));<br><br>

   	б)<br>
   		$request = "SELECT fio,email,birth,sex,limb,about,checkbox FROM form WHERE user_id = '$user_id'";<br>
        $result = $db -> prepare($request);<br>
        $result ->execute();<br>
        $data = $result->fetch(PDO::FETCH_ASSOC);<br><br>

*Устранение CSRF(Подделка межсайтовых запросов, также известен как XSRF)-уязвимостей:<br><br>
	
а) Генерация уникального токена:<br>

		function generate_form_token(  ) <br>{
    		return $_SESSION['csrf_token'] = substr( str_shuffle( 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM' ), 0, 10 );
		}<br><br>
б) Проверка на совпадение CSRF-токена сессии с токеном формы для исключения отправки формы, введенной злоумышленниками на другом сайте:<br>

		if ($errors && !isset($_SESSION['csrf_token']) && !$_SESSION['csrf_token'] == @$_POST['csrf_token']) <br>{
        	header('Location: index.php');<br>
        	exit();
    	}<br><br>
		в) Скрытое поле в форме, в котором генерируется этот токен:<br>

		//<input name="csrf_token" type="hidden" value="'.generate_form_token().'"/>//<br><br>

*XSS(XSS (межсайтовый скриптинг) – одна из разновидностей атак на веб-системы, которая <br>
подразумевает внедрение вредоносного кода на определенную страницу сайта и взаимодействие этого кода <br>
с удаленным сервером злоумышленников при открытии страницы пользователем.)-уязвимости:<br><br>
а) Использование метода strip_tags()-сравнивает старое значение с новым, чтобы увидеть, равны ли они:<br>
		$values['fio'] = strip_tags($data['fio']);<br>
        $values['email'] = strip_tags($data['email']);<br>
        $values['year_value'] = strip_tags($data['birth']);<br>
        $values['sex_value'] = strip_tags($data['sex']);<br>
        $values['limb_value'] = $data['limb'];<br>
        $values['bio_value'] = strip_tags($data['about']);<br>
        $values['check_value'] = strip_tags($data['checkbox']);<br><br>
б) Использование метода preg_match()— Выполняет проверку на соответствие регулярному выражению:<br>
    	if (empty($fio) || (preg_match("/^[a-z0-9_-]{2,20}$/i", $fio))) <br>{
        	setcookie('fio_error', '1', time() + 24 * 60 * 60);<br>
        	$errors = TRUE;
    	}<br><br>
в) Использование метода htmlspecialchars() (экранизирование данных для предотвращкния <br>
их интерпретацию как исполняемых инструкций в новом контексте) <br>
  для исключения спецсимволов (кавычек и т.д.) из ввода:<br>
    	$fio = htmlspecialchars($_POST['fio'], ENT_QUOTES, 'UTF-8');<br>
    	$email = htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8');<br>
    	$birthyear = htmlspecialchars($_POST['birthyear'], ENT_QUOTES, 'UTF-8');<br>
    	$bio = htmlspecialchars($_POST['textarea1'], ENT_QUOTES, 'UTF-8');<br>
		<br><br>
  <h1 class="fig">Задание 7</h1>
  <h3>CREATE TABLE abilities</h3>
  <h3>(user_id varchar(128) NOT NULL,</h3>
  <h3>abil_value varchar(128)NOT NULL);</h3>
  <br>
<h3>CREATE TABLE form</h3>
<h3>(user_id varchar(128),</h3>
<h3>fio varchar(20),</h3>
<h3>birth int(10),</h3>
<h3>email varchar(30),</h3>
<h3>sex varchar(10),</h3>
<h3>limb int,</h3>
<h3>about varchar(128),</h3>
<h3>checkbox int, </h3>
<h3>PRIMARY KEY(user_id));</h3>
<br>
<h3>CREATE TABLE </h3>
<h3>users(user_id int(10)</h3>
<h3> UNSIGNED AUTO_INCREMENT,</h3>
 <h3>login varchar(128),</h3>
<h3> hash varchar(128),</h3>
 <h3>PRIMARY KEY(user_id));</h3>
 <br>
<h3>CREATE TABLE</h3>
<h3> admin(login varchar(128) NOT NULL,</h3> 
 <h3>hash varchar(128) NOT NULL,</h3>
 <h3>PRIMARY KEY(login));</h3>
</body>